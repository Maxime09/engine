use crate::EntityManager;
use crate::ComponentManager;
use std::sync::Arc;

pub trait System: Send + Sync{
    fn run(&mut self, entities: &EntityManager, components: Arc<&ComponentManager>);
}

pub struct SystemManager{
    system: Vec<Box<dyn System + 'static>>,
    main_thread_system: Vec<Box<dyn System + 'static>>
}

impl SystemManager{
    pub fn new() -> Self{
        SystemManager{
            system: Vec::new(),
            main_thread_system: Vec::new(),
        }
    }

    pub fn create_system<S: System>(&mut self, system: S) where S: 'static{
        self.system.push(Box::new(system));
    }

    pub fn add_system_to_main_thread<S: System>(&mut self, system: S) where S: 'static{
        self.main_thread_system.push(Box::new(system));
    }

    pub fn run(&mut self, entities: &EntityManager, components: &ComponentManager){
        let comps = Arc::new(components);
        crossbeam::scope(|s|{
            for sys in self.system.iter_mut(){
                let local_comps = Arc::clone(&comps);
                s.spawn(move |_| {
                    sys.as_mut().run(entities, local_comps);
                });
            }
        }).unwrap();
        for sys in self.main_thread_system.iter_mut(){
            sys.as_mut().run(entities, Arc::clone(&comps));
        }
    }
}

impl Default for SystemManager{
    fn default() -> Self {
        Self::new()
    }
}
