use std::collections::HashMap;
use typemap::{TypeMap, Key};
use std::sync::Mutex;

pub trait Component {
}


pub struct Comp<T> where T: Component + 'static{
    pub component: T,
}

impl<T> Key for Comp<T> where T: Component + 'static{
    type Value = HashMap<usize, Mutex<T>>;
}

pub struct ComponentManager {
    comps: TypeMap,
}

impl ComponentManager{
    pub fn new() -> Self{
        ComponentManager{
            comps: TypeMap::new(),
        }
    }

    pub fn register_component<C>(&mut self) where C: Component + 'static{
        self.comps.insert::<Comp<C>>(HashMap::new());
    }

    pub fn add_component<C>(&mut self, entity: usize, component: C) where C: Component + 'static{
        self.comps.get_mut::<Comp<C>>().unwrap_or_else(|| panic!("Trying to add an unregistered component to an entity")).insert(entity, Mutex::new(component));
    }

    pub fn get_component<C>(&self, entity_id: usize) -> Option<&Mutex<C>> where C: Component + 'static{
        self.comps.get::<Comp<C>>().unwrap_or_else(|| panic!("Trying to access unregistered component")).get(&entity_id)
    }

    pub fn entity_has_component<C>(&self, entity_id: usize) -> bool where C: Component + 'static{
        self.comps.get::<Comp<C>>().unwrap_or_else(|| panic!("Trying to use an unregistered component")).contains_key(&entity_id)
    }

}

impl Default for ComponentManager{
    fn default() -> Self{
        Self::new()
    }
}

unsafe impl Sync for ComponentManager{}
unsafe impl Send for ComponentManager{}
