use std::collections::HashMap;
use crate::{Component, ComponentManager};

pub struct EntityManager{
    entities: HashMap<&'static str, usize>,
}

impl EntityManager{
    pub fn new() -> Self{
        EntityManager{
            entities: HashMap::<&str, usize>::new(),
        }
    }

    fn get_biggest_id(&self) -> usize{
        let mut max = 0;
        for val in self.entities.values() {
            if *val > max {
                max = *val;
            }
        }
        max
    }

    pub fn create_entity(&mut self, name: &'static str) -> usize{
        let id = self.get_biggest_id() + 1;
        self.entities.insert(name, id);
        id
    }

    pub fn get_entity(&self, name: &str) -> usize{
        *self.entities.get(name).expect(&format!("No entity \"{}\" found", name))
    }

    pub fn find_comp<C>(&self, components: &ComponentManager) -> Option<usize> where C: Component + 'static{
        let mut r = None;
        for id in self.entities.values(){
            if components.entity_has_component::<C>(*id){
                r = Some(*id);
                break;
            }
        }
        r
    }

    pub fn comp_iter<C>(&self, components: &ComponentManager) -> IdComponentIterator where C: Component + 'static{
        let mut values: Vec<usize> = Vec::new();
        for id in self.entities.values(){
            if components.entity_has_component::<C>(*id){
                values.push(*id);
            }
        }
        IdComponentIterator{
            values
        }
    }

    pub fn comp_pair_iter<C, D>(&self, components: &ComponentManager) -> IdComponentIterator where C: Component + 'static, D: Component +'static{
        let mut values: Vec<usize> = Vec::new();
        for id in self.entities.values(){
            if components.entity_has_component::<C>(*id) && components.entity_has_component::<D>(*id){
                values.push(*id);
            }
        }
        IdComponentIterator{
            values
        }
    }
}

impl Default for EntityManager{
    fn default() -> Self {
        Self::new()
    }
}

pub struct IdComponentIterator{
    values: Vec<usize>
}

impl Iterator for IdComponentIterator{
    type Item = usize;

    fn next(&mut self) -> Option<Self::Item>{
        self.values.pop()
    }
}
