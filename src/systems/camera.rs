use crate::components::Camera;
use ecs::*;
use crate::components::GlobalCamera;
use std::sync::Arc;

///Select the camera to render
pub struct SysCameraUpdate;

impl System for SysCameraUpdate {
    fn run(&mut self, entities: &EntityManager, components: Arc<&ComponentManager>){
        let mut id_used_camera: Option<usize> = None;
        for id in entities.comp_iter::<Camera>(*components) {
            if components.get_component::<Camera>(id).unwrap().lock().unwrap().is_used(){
                id_used_camera = Some(id);
                break;
            }
        }
        let internal_global = entities.get_entity("InternalGlobal");
        components.get_component::<GlobalCamera>(internal_global).expect("Camera systems not initialized").lock().unwrap().set_cam_id(id_used_camera);
    }
}
