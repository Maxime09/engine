use crate::components::{Object, GlobalCamera, Camera, Texture};
use crate::render::{ProjMatrix, GlComp, ProgramComp};
use std::ffi::CString;
use ecs::*;
use crate::vertices::{ColorVertex, UVVertex};
use render_lib::{gl, Object3D};
use crate::render as render_core;
use std::sync::Arc;

///Renderer to use with [Color Vertex](../vertices/struct.ColorVertex.html)
pub struct SysColorVertexRenderer;

impl System for SysColorVertexRenderer{

    fn run(&mut self, entities: &EntityManager, comps: Arc<&ComponentManager>){
        // Get the internal engine entity
        let internal_global = entities.get_entity("InternalGlobal");
        // Get the id of the entity containing the used camera
        let cam_id = comps.get_component::<GlobalCamera>(internal_global).expect("Camera systems not initialized.").lock().unwrap().get_cam_id();

        let gl_lock = comps.get_component::<GlComp>(internal_global).expect("No gl instance found").lock().unwrap();
        let gl = gl_lock.get();

        let view_matrix = comps.get_component::<Camera>(cam_id).unwrap().lock().unwrap().get_cam().get_view_matrix().to_homogeneous();
        let mut objs: Vec<Object3D<ColorVertex>> = Vec::new();
        for id in entities.comp_iter::<Object>(*comps){
            let lock = comps.get_component::<Object>(id).unwrap().lock().unwrap();
            if let Some(obj) = lock.get_inner_color(){
                objs.push(obj.clone(gl));
            }
        }
        let projection = comps.get_component::<ProjMatrix>(internal_global).expect("No projection matrix found.").lock().unwrap().get();
        let prg_lock = comps.get_component::<ProgramComp>(internal_global).expect("No Shaders program found").lock().unwrap();
        let program = prg_lock.get("color");
        render_core::render_v(&objs, projection, view_matrix, program, gl);
    }
}

///Renderer to use with [UV Vertex](../vertices/struct.UVVertex.html)
///the entities containing the object component must contain a
///[Texture](../components/struct.Texture.html) component too.
pub struct SysUVVertexRenderer;

impl System for SysUVVertexRenderer{
    fn run(&mut self, entities: &EntityManager, components: Arc<&ComponentManager>) {
        let internal_global = entities.get_entity("InternalGlobal");

        let cam_id = components.get_component::<GlobalCamera>(internal_global).expect("Camera systems not initialized.").lock().unwrap().get_cam_id();

        let view_matrix = components.get_component::<Camera>(cam_id).unwrap().lock().unwrap().get_cam().get_view_matrix().to_homogeneous();
        let gl_lock = components.get_component::<GlComp>(internal_global).expect("No gl instance found").lock().unwrap();
        let gl = gl_lock.get();

        let mut objs: Vec<Object3D<UVVertex>> = Vec::new();
        let mut textures: Vec<render_lib::gl::types::GLuint> = Vec::new();
        let mut objs_count = 0;
        for id in entities.comp_pair_iter::<Object, Texture>(*components){
            let lock = components.get_component::<Object>(id).unwrap().lock().unwrap();
            if let Some(obj) = lock.get_inner_uv(){
                objs.push(obj.clone(gl));
                textures.push(components.get_component::<Texture>(id).unwrap().lock().unwrap().texture);
                objs_count += 1;
            }
        }
        let projection = components.get_component::<ProjMatrix>(internal_global).expect("No projection matrix found.").lock().unwrap().get();

        let prg_lock = components.get_component::<ProgramComp>(internal_global).expect("No Shaders program found").lock().unwrap();
        let program = prg_lock.get("UV");
        let mvp_id = program.get_uniform_location(gl, CString::new("MVP").unwrap());
        let vp_matrix = projection*view_matrix;
        for i in 0..objs_count{
            unsafe{
                gl.BindTexture(gl::TEXTURE_2D, textures[i]);
            }
            let mvp = vp_matrix * objs[i].get_model_matrix();
            render_lib::render_object(&objs[i], &mvp, program, mvp_id, gl);
        }
    }
}
