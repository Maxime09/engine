use ecs::System;
use crate::components::Transform;
use crate::components::Object;

use std::sync::Arc;

///A system needed by the [Transform](../components/struct.Transform.html) component.
pub struct SysTransform;

impl System for SysTransform{
    fn run(&mut self, entities: &ecs::EntityManager, components: Arc<&ecs::ComponentManager>) {
        for id in entities.comp_pair_iter::<Transform, Object>(*components){
            let (position, rotation, scale) = {
                let transform = components.get_component::<Transform>(id).unwrap().lock().unwrap();
                (transform.position, transform.rotation, transform.scale)
            };
            let mut obj = components.get_component::<Object>(id).unwrap().lock().unwrap();
            obj.set_pos(position);
            obj.set_rotation(rotation);
            obj.set_scale(scale);
        }
    }
}
