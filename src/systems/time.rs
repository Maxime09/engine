use ecs::System;
use crate::components::Time;
use std::sync::Arc;

///A system that update [Time](../components/struct.Time.html) component every frame
pub struct SysTime;

impl System for SysTime{
    fn run(&mut self, entities: &ecs::EntityManager, components: Arc<& ecs::ComponentManager>) {
        for id in entities.comp_iter::<Time>(*components){
            components.get_component::<Time>(id).unwrap().lock().unwrap().update();
        }
    }

}
