use ecs::System;
use std::sync::Arc;
use crate::components::physics::{World, RigidBody};
use crate::components::Transform;
use crate::components::Time;
use render_lib::na::Vector3;

pub struct PhysicalWorldSys{}

impl System for PhysicalWorldSys{
    fn run(&mut self, entities: &ecs::EntityManager, components: Arc<&ecs::ComponentManager>){
        for id in entities.comp_iter::<World>(*components){
            let time_id = entities.find_comp::<Time>(*components).unwrap();
            let delta_time = components.get_component::<Time>(time_id).unwrap().lock().unwrap().get_delta_time();
            components.get_component::<World>(id).unwrap().lock().unwrap().set_timestep(delta_time);
            components.get_component::<World>(id).unwrap().lock().unwrap().update();
        }
    }
}

pub struct RigidBodySys {}

impl System for RigidBodySys{
    fn run(&mut self, entities: &ecs::EntityManager, components: Arc<&ecs::ComponentManager>){
        let w_id = entities.find_comp::<World>(*components).expect("Cannot find physical world");
        for id in entities.comp_iter::<RigidBody>(*components){
            let rb = components.get_component::<RigidBody>(id).unwrap().lock().unwrap();
            let mut w = components.get_component::<World>(w_id).unwrap().lock().unwrap();
            let rb_inner = rb.inner_rigid_body_mut(w.get_mut_body_set()).expect("Unitialized rigid body used");
            let iso = rb_inner.position();
            let mut transform = components.get_component::<Transform>(id).expect("RigidBody need Transform component").lock().unwrap();
            transform.position = iso.translation.vector;
            let (x, y, z) = iso.rotation.euler_angles();
            transform.rotation = Vector3::new(x, y, z);
        }
    }
}
