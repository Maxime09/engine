use render_lib::*;
use render_lib::sdl;
use sdl::event::{Event, WindowEvent};
use ecs::{SystemManager, EntityManager, ComponentManager};
#[cfg(feature = "render")]
use crate::components::GlobalCamera;
#[cfg(feature = "render")]
use crate::render::{ProjMatrix, GlComp, ProgramComp};
use crate::shader::*;
use std::collections::HashMap;
use render_lib::render_gl::Shader;
#[cfg(feature = "input")]
use crate::components::Input;

/// Represent an instance of the game
pub struct Game{
    window: Window,
    gl: gl::Gl,
    event_pump: sdl::EventPump,
    system_manager: SystemManager,
    entities: EntityManager,
    components: ComponentManager,
    #[cfg(feature = "render")]
    projection: Projection,
}

impl Game{
    /// Create a new Game instance
    ///
    /// # Argument
    ///
    /// * `title` - A string containing the window's title
    ///
    /// # Return type
    ///
    /// Return an instance of Game
    ///
    /// # Example
    ///
    /// ```
    /// use engine::Game;
    /// let game = Game::new("Example");
    /// ```
    pub fn new(title: &str) -> Game{
        let window = Window::new(title, 900, 700);

        // Create an instance of gl;
        let gl = window.init_gl();


        // Init system, entity and component manager
        let system_manager = SystemManager::new();
        let mut entities = EntityManager::new();
        let mut components = ComponentManager::new();

        // Create global entity containing components needed for the internal work of the engine
        let internal_global = entities.create_entity("InternalGlobal");

        #[cfg(feature = "render")]
        {
            components.register_component::<ProjMatrix>();
            components.register_component::<GlComp>();
            components.register_component::<ProgramComp>();
        }

        #[cfg(feature = "input")]
        components.register_component::<Input>();

        // Create and add gl reference and projection matrix to the internal global entity
        #[cfg(feature = "render")]
        let projection = Projection::new(900, 700, std::f32::consts::PI/4.0, 0.1, 100.0, &gl);

        #[cfg(feature = "render")]
        {
            components.add_component(internal_global, ProjMatrix{ mat: projection.get_projection_matrix().to_homogeneous() });
            components.add_component(internal_global, GlComp{gl: gl.clone()});
            components.add_component(internal_global, ProgramComp{programs: HashMap::new()});

            components.register_component::<GlobalCamera>();
            components.add_component(internal_global, GlobalCamera{
                cam_entity_name: None,
            });
        }

        #[cfg(feature = "input")]
        {
            let input = entities.create_entity("Input");
            components.add_component(input, Input::new());
        }

        let event_pump = window.get_events();
        Game {
            window,
            gl,
            event_pump,
            system_manager,
            entities,
            components,
            #[cfg(feature = "render")]
            projection,
        }
    }

    /// Create a new entity and add it to the game
    ///
    /// # Argument
    ///
    /// * `name` - A string containig the name of the entity
    ///
    /// # Return type
    ///
    /// Return a `usize` containing the entity id
    ///
    /// # Example
    ///
    /// ```
    /// use engine::Game;
    /// let game = Game::new("Game");
    /// let entity = game.create_entity("Entity");
    /// ```
    pub fn create_entity(&mut self, name: &'static str) ->  usize {
        self.entities.create_entity(name)
    }

    /// Return an immutable reference to the system manager
    ///
    /// # Example
    ///
    /// ```
    /// use engine::Game;
    /// let game = ....
    /// let system_manager = game.get_system_manager();
    /// ```
    pub fn get_system_manager(&self) -> &SystemManager{
        &self.system_manager
    }

    /// Return a mutable reference to the system manager
    ///
    /// # Exemple
    ///
    /// ```
    /// use engine::Game;
    /// let game = ....
    /// let system_manager = game.get_mut_system_manager();
    /// ```
    pub fn get_mut_system_manager(&mut self) -> &mut SystemManager{
        &mut self.system_manager
    }

    /// Return an immutable reference to the entity manager
    ///
    /// # Example
    ///
    /// ```
    /// use engine::Game;
    /// let game = ....
    /// let system_manager = game.get_entity_manager();
    /// ```
    pub fn get_entity_manager(&self) -> &EntityManager{
        &self.entities
    }

    /// Return a mutable reference to the entity manager
    ///
    /// # Example
    ///
    /// ```
    /// use engine::Game;
    /// let game = ....
    /// let system_manager = game.get_mut_entity_manager();
    /// ```
    pub fn get_mut_entity_manager(&mut self) -> &mut EntityManager{
        &mut self.entities
    }

    /// Return an immutable reference to the component manager
    ///
    /// # Example
    ///
    /// ```
    /// use engine::Game;
    /// let game = ....
    /// let system_manager = game.get_component_manager();
    /// ```
    pub fn get_component_manager(&self) -> &ComponentManager{
        &self.components
    }

    /// Return a mutable reference to the component manager
    ///
    /// # Example
    ///
    /// ```
    /// use engine::Game;
    /// let game = ....
    /// let system_manager = game.get_mut_component_manager();
    /// ```
    pub fn get_mut_component_manager(&mut self) -> &mut ComponentManager{
        &mut self.components
    }

    /// Return the gl instance
    ///
    /// # Example
    ///
    /// ```
    /// use engine::Game;
    /// let game = ....
    /// let gl = game.get_gl();
    /// ```
    pub fn get_gl(&self) -> gl::Gl{
        self.gl.clone()
    }

    /// Add a shader so it can be accessed by the Game
    ///
    /// # Arguments
    ///
    /// `key` - A `&'static str` used as an identifier
    ///
    /// `vertex_shader` - The vertex shader as `Shader` struct
    ///
    /// `fragment_shader` - The vertex shader as 'Shader` struct
    ///
    /// # Example
    /// ```
    /// use engine::render_lib::render_gl::Shader;
    /// use std::ffi::CString;
    ///
    /// let string_source = CString::new(include_str!("vertex_shader.glsl")).unwrap();
    /// let source = string_source.as_c_str();
    /// let vertex_shader = Shader::from_vert_source(&game.get_gl(), source);
    ///
    /// let string_source = CString::new(include_str!("fragment_shader.glsl")).unwrap();
    /// let source = string_source.as_c_str();
    /// let fragment_shader = Shader::from_frag_source(&game.get_gl(), source);
    ///
    /// game.add_shader("Example", vertex_shader, fragment_shader);
    /// ```
    #[cfg(feature = "render")]
    pub fn add_shader(&mut self, key: &'static str, vertex_shader: Shader, fragment_shader: Shader){
        let internal_global = self.entities.get_entity("InternalGlobal");
        let shaders = [vertex_shader, fragment_shader];
        let program = render_lib::Program::from_shaders(&self.gl, &shaders);
        self.components.get_component::<ProgramComp>(internal_global).unwrap().lock().unwrap().add_shader(key, program);
    }

    /// Add default shader for color vertex with identifier "color"
    #[cfg(feature = "render")]
    pub fn init_color_vertex_shader(&mut self){
        self.add_shader("color", color_vertex_vertex_shader(&self.gl), color_vertex_fragment_shader(&self.gl));
    }

    /// Add default shader for uv vertex with identifier "UV"
    #[cfg(feature = "render")]
    pub fn init_uv_vertex_shader(&mut self){
        self.add_shader("UV", uv_vertex_vertex_shader(&self.gl), uv_vertex_fragment_shader(&self.gl));
    }

    pub fn get_window(&self) -> &Window{
        &self.window
    }

    pub fn get_mut_window(&mut self) -> &mut Window{
        &mut self.window
    }

    /// Use this method after initialisation to launch the game
    ///
    /// # Example
    /// ```
    /// let mut game = Game::new("Game");
    ///
    /// game.init_camera_components();
    ///
    /// //create every systems and components
    /// ....
    ///
    /// game.run();
    /// ```
    pub fn run(&mut self){
        let color_buffer = render_gl::ColorBuffer::from_color(na::Vector3::new(1.0, 1.0, 1.0));
        color_buffer.set_used(&self.gl);

        'main: loop{


            #[cfg(feature = "input")]
            let input_entity = {
                let input = self.entities.get_entity("Input");
                self.components.get_component::<Input>(input).unwrap().lock().unwrap().update();
                input
            };

            // Handle Event
            for event in self.event_pump.poll_iter() {
                match event {
                    Event::Quit {..} => {
                        break 'main
                    },
                    #[cfg(feature = "render")]
                    Event::Window { win_event: WindowEvent::Resized(w, h), .. } | Event::Window {win_event: WindowEvent::SizeChanged(w, h), ..} => {
                        self.projection.set_aspect(w, h, &self.gl);
                        self.components.get_component::<ProjMatrix>(self.entities.get_entity("InternalGlobal")).expect("Error during engine initialization").lock().unwrap().set(self.projection.get_projection_matrix().to_homogeneous());
                    },
                    #[cfg(feature = "input")]
                    Event::KeyDown { keycode: Some(code), ..} => {
                        let mut input = self.components.get_component::<Input>(input_entity).unwrap().lock().unwrap();
                        input.key_pressed(code)
                    },
                    #[cfg(feature = "input")]
                    Event::KeyUp { keycode: Some(code), ..} => {
                        let mut input = self.components.get_component::<Input>(input_entity).unwrap().lock().unwrap();
                        input.key_released(code)
                    },
                    _ => {}
                }
            }
            color_buffer.clear(&self.gl);
            self.system_manager.run(&self.entities, &mut self.components);

            self.window.swap_window();
        }
    }

}
