use ecs::Component;
use render_lib::gl;
use crate::utils::image_loader;

///This is a component holding a texture,
///it is needed to use the [default renderer for UV vertex](../systems/struct.SysUVVertexRenderer.html)
pub struct Texture{
    pub texture: render_lib::gl::types::GLuint,
}

impl Component for Texture{
}

impl Texture{
    ///Create a new component texture its source file
    pub fn new(src: &str, gl: &gl::Gl) -> Self{
        Texture{texture: image_loader::load_texture_from_image(src, gl)}
    }
}
