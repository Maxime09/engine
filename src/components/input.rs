use std::collections::HashSet;
use ecs::Component;
use render_lib::sdl::keyboard::Keycode;

pub struct Input{
    key_first: HashSet<Keycode>,
    key_pressed: HashSet<Keycode>,
    key_released: HashSet<Keycode>,
}

impl Input{
    pub fn new() -> Self{
        Input{
            key_first: HashSet::new(),
            key_pressed: HashSet::new(),
            key_released: HashSet::new(),
        }
    }

    pub fn update(&mut self){
        self.key_released.clear();
        for key in self.key_first.iter(){
            self.key_pressed.insert(*key);
        }
        self.key_first.clear();
    }

    pub fn get_key(&self, code: Keycode) -> bool{
        self.key_first.contains(&code) || self.key_pressed.contains(&code)
    }

    pub fn get_key_down(&self, code: Keycode) -> bool{
        self.key_first.contains(&code)
    }

    pub fn get_key_up(&self, code: Keycode) -> bool{
        self.key_released.contains(&code)
    }

    pub fn key_pressed(&mut self, code: Keycode){
        if !self.get_key(code){
            self.key_first.insert(code);
        }
    }

    pub fn key_released(&mut self, code: Keycode){
        self.key_pressed.remove(&code);
        self.key_first.remove(&code);
        self.key_released.insert(code);
    }
}

impl Default for Input{
    fn default() -> Self {
        Self::new()
    }
}

impl Component for Input{}
