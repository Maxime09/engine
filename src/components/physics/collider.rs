use ncollide3d::shape::ShapeHandle;
use nphysics3d::object::ColliderDesc;
use nphysics3d::object::BodyPartHandle;
use nphysics3d::object::DefaultColliderHandle;
use nphysics3d::object::DefaultColliderSet;
use crate::components::physics::rigid_body::RigidBody;
use ecs::Component;

pub struct Collider{
    handle: DefaultColliderHandle,
}

impl Collider{
    pub fn new(shape: ShapeHandle<f32>, rb: &RigidBody, collider_set: &mut DefaultColliderSet<f32>) -> Self{
        let collider = ColliderDesc::new(shape)
            .build(BodyPartHandle(rb.get_handle(), 0));
        let handle = collider_set.insert(collider);
        Collider{
            handle
        }
    }
}

impl Component for Collider{}
