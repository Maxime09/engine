use nphysics3d::object::DefaultBodyHandle as Handle;
use nphysics3d::object::RigidBodyDesc;
use nphysics3d::object::DefaultBodySet;
use ecs::Component;

pub struct RigidBody{
    handle: Handle
}

impl RigidBody{
    pub fn new(body_set: &mut DefaultBodySet<f32>) -> Self{
        let rigid_body = RigidBodyDesc::new()
            .build();
        let handle = body_set.insert(rigid_body);
        RigidBody{
            handle,
        }
    }

    pub fn inner_rigid_body<'a>(&self, body_set: &'a DefaultBodySet<f32>) -> Option<&'a nphysics3d::object::RigidBody<f32>>{
        body_set.rigid_body(self.handle)
    }

    pub fn inner_rigid_body_mut<'a>(&self, body_set: &'a mut DefaultBodySet<f32>) -> Option<&'a mut nphysics3d::object::RigidBody<f32>>{
        body_set.rigid_body_mut(self.handle)
    }

    pub fn get_handle(&self) -> Handle{
        self.handle
    }
}


impl Component for RigidBody{}
