use render_lib::na;
use na::Vector3;
use nphysics3d::object::{DefaultBodySet, DefaultColliderSet};
use nphysics3d::force_generator::DefaultForceGeneratorSet;
use nphysics3d::joint::DefaultJointConstraintSet;
use nphysics3d::world::{DefaultMechanicalWorld, DefaultGeometricalWorld};
use ecs::Component;

pub struct World{
    mech: DefaultMechanicalWorld<f32>,
    geo: DefaultGeometricalWorld<f32>,
    bodies: DefaultBodySet<f32>,
    colliders: DefaultColliderSet<f32>,
    joint_constraints: DefaultJointConstraintSet<f32>,
    force_generators: DefaultForceGeneratorSet<f32>,
}

impl World{
    pub fn new(gravity: Vector3<f32>) -> Self{
        let mechanical_world = DefaultMechanicalWorld::new(gravity);
        let geometrical_world = DefaultGeometricalWorld::new();

        let bodies = DefaultBodySet::new();
        let colliders = DefaultColliderSet::new();
        let joint_constraints = DefaultJointConstraintSet::new();
        let force_generators = DefaultForceGeneratorSet::new();

        World{
            mech: mechanical_world,
            geo: geometrical_world,
            bodies,
            colliders,
            joint_constraints,
            force_generators,
        }
    }

    pub fn update(&mut self){
        self.mech.step(
            &mut self.geo,
            &mut self.bodies,
            &mut self.colliders,
            &mut self.joint_constraints,
            &mut self.force_generators,
        );
    }

    pub fn set_timestep(&mut self, delta_time: f32){
        self.mech.set_timestep(delta_time);
    }

    pub fn get_body_set(&self) -> &DefaultBodySet<f32> {
        &self.bodies
    }

    pub fn get_mut_body_set(&mut self) -> &mut DefaultBodySet<f32> {
        &mut self.bodies
    }

    pub fn get_collider_set(&self) -> &DefaultColliderSet<f32> {
        &self.colliders
    }

    pub fn get_mut_collider_set(&mut self) -> &mut DefaultColliderSet<f32> {
        &mut self.colliders
    }
}

impl Default for World{
    fn default() -> Self{
        Self::new(Vector3::new(0.0, -9.81, 0.0))
    }
}

impl Component for World{}
