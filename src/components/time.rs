use ecs::Component;
use render_lib::TimeCounter;

/// A component that store time
pub struct Time{
    counter: TimeCounter,
    delta_time: f32,
}

impl Component for Time{}

impl Time{
    ///Create a new component Time
    pub fn new() -> Self{
        Time{counter: TimeCounter::new(), delta_time: 0f32}
    }

    ///Update time passed since last frame
    ///(automatically called by the [Time system](../systems/struct.SysTime.html))
    pub fn update(&mut self){
        self.delta_time = self.counter.get_delta_time();
    }

    ///Return the time passed since the last frame
    pub fn get_delta_time(&self) -> f32{
        self.delta_time
    }
}

impl Default for Time{
    fn default() -> Self {
        Self::new()
    }
}
