use ecs::Component;
use crate::vertices::{ColorVertex, UVVertex};
use render_lib::Object3D;
use render_lib::na::Vector3;


/// Represent a 3D Object
pub enum Object{
    Color(Object3D<ColorVertex>),
    UV(Object3D<UVVertex>)
}

impl Object{
    /// Create a new object that use the [SysColorVertexRenderer](../systems/struct.SysColorVertexRenderer.html) renderer
    ///
    /// # Arguments
    ///
    /// `Vertices` - a vector containing the object's vertices (Vec<ColorVertex>)
    /// `gl` - a reference to the gl struct
    pub fn new_color(vertices: &[ColorVertex], gl: &crate::render_lib::gl::Gl) -> Self{
        Object::Color(Object3D::new(vertices, gl))
    }

    /// Create a new object that use the [SysUVVertexRenderer](../systems/struct.SysUVVertexRenderer.html) renderer
    /// # Arguments
    ///
    /// `Vertices` - a vector containing the object's vertices (Vec<UVVertex>)
    /// `gl` - a reference to the gl struct
    pub fn new_uv(vertices: &[UVVertex], gl: &crate::render_lib::gl::Gl) -> Self{
        Object::UV(Object3D::new(vertices, gl))
    }

    /// Get a reference to the inner `Object3D<ColorVertex>`
    /// return None if another type of vertex is used
    pub fn get_inner_color(&self) -> Option<&Object3D<ColorVertex>>{
        if let Self::Color(obj) = self{
            Some(obj)
        }else{
            None
        }
    }

    /// Get a mutable reference to the inner `Object3D<ColorVertex>`
    /// return None if another type of vertex is used
    pub fn get_mut_inner_color(&mut self) -> Option<&mut Object3D<ColorVertex>>{
        if let Self::Color(obj) = self{
            Some(obj)
        }else{
            None
        }
    }

    /// Get a reference to the inner `Object3D<UVVertex>`
    /// return None if another type of vertex is used
    pub fn get_inner_uv(&self) -> Option<&Object3D<UVVertex>>{
        if let Self::UV(obj) = self{
            Some(obj)
        }else{
            None
        }
    }

    /// Get a mutable reference to the inner `Object3D<UVVertex>`
    /// return None if another type of vertex is used
    pub fn get_mut_inner_uv(&mut self) -> Option<&mut Object3D<UVVertex>>{
        if let Self::UV(obj) = self{
            Some(obj)
        }else{
            None
        }
    }

    /// Set the position of the object
    pub fn set_pos(&mut self, pos: Vector3<f32>){
        match self{
            Object::Color(a) => a.set_pos(pos),
            Object::UV(a) => a.set_pos(pos),
        }
    }

    /// Get the position of the object
    pub fn get_pos(&self) -> Vector3<f32>{
        match self{
            Object::Color(a) => a.get_pos(),
            Object::UV(a) => a.get_pos(),
        }
    }

    /// Set the scale of the object
    pub fn set_scale(&mut self, scale: Vector3<f32>){
        match self{
            Object::Color(a) => a.set_scale(scale),
            Object::UV(a) => a.set_scale(scale),
        }
    }

    /// Get the scale of the object
    pub fn get_scale(&self) -> Vector3<f32>{
        match self{
            Object::Color(a) => a.get_scale(),
            Object::UV(a) => a.get_scale(),
        }
    }

    /// Set the rotation of the object
    pub fn set_rotation(&mut self, rotation: Vector3<f32>){
        match self{
            Object::Color(a) => a.set_rotation(rotation),
            Object::UV(a) => a.set_rotation(rotation),
        }
    }

    /// Get the rotation of the object
    pub fn get_rotation(&self) -> Vector3<f32>{
        match self{
            Object::Color(a) => a.get_rotation(),
            Object::UV(a) => a.get_rotation(),
        }
    }
}

impl Component for Object{
}
