use ecs::Component;


/// This is a component containing a Camera used to render the scene
pub struct Camera{
    cam: render_lib::render_gl::Camera,
    used: bool,
}

impl Camera{
    /// Create a new Camera
    ///
    /// # Arguments
    ///
    /// * `x`,`y` and `z` - coordinate of the camera (f32)
    /// * `used` - Whether the camera is currently used a the main camera (bool)
    ///
    /// # Return value
    ///
    /// a camera component
    ///
    /// # Example:
    ///
    /// ```
    /// // Create a new camera at the coordinates (0, 1, 2) that isn't the main camera
    /// let camera = Camera::new(0, 1, 2, false);
    /// ```
    pub fn new(x: f32, y: f32, z: f32, used: bool) -> Self{
        Camera{
            cam: render_lib::render_gl::Camera::new_look_at_origin(x, y, z),
            used,
        }
    }

    /// Return a reference to the internal Camera structure
    ///
    /// # Return value
    ///
    /// A reference to the internal Camera structure
    pub fn get_cam(&self) -> &render_lib::render_gl::Camera{
        &self.cam
    }

    /// Same as get_cam but return a mutable reference
    pub fn get_mut_cam(&mut self) -> &mut render_lib::render_gl::Camera{
        &mut self.cam
    }

    /// Return whether this Camera is the main one or not as a boolean
    pub fn is_used(&self) -> bool{
        self.used
    }

    /// Set whether this Camera is the main one or not
    ///
    /// # Arguments
    ///
    /// `value` - Is this the cam the main one (bool)
    pub fn set_used(&mut self, value: bool){
        self.used = value;
    }
}

impl Component for Camera{
}

#[doc(hidden)]
pub struct GlobalCamera{
    pub cam_entity_name: Option<usize>
}

impl GlobalCamera{
    pub fn get_cam_id(&self) -> usize{
        self.cam_entity_name.unwrap_or_else(|| panic!("No Camera"))
    }

    pub fn set_cam_id(&mut self, value: Option<usize>){
        self.cam_entity_name = value;
    }
}

impl Component for GlobalCamera{}
