use ecs::Component;
use render_lib::na;

/// A component to move, rotate and scale objects,
/// need the [Transform system](../systems/struct.SysTransform.html).
pub struct Transform{
    pub position: na::Vector3<f32>,
    pub rotation: na::Vector3<f32>,
    pub scale: na::Vector3<f32>,
}

impl Component for Transform{
}

impl Transform{
    ///Create a new transform with an initial poisition at the origin, no rotation
    ///and no scale.
    pub fn new() -> Self{
        Transform{position: na::Vector3::new(0f32, 0f32, 0f32), rotation: na::Vector3::new(0f32, 0f32, 0f32), scale: na::Vector3::new(1f32, 1f32, 1f32)}
    }
}

impl Default for Transform{
    fn default() -> Self {
        Self::new()
    }
}
