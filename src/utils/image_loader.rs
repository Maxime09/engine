use image::open;
use std::path::Path;
use render_lib::gl::Gl;
use render_lib::gl;

/// Load a texture inside gl from an image file
///
/// # Arguments
///
/// * `location` - A &str indicating where the image is located
///
/// * `gl` - The instance of gl used in the game
///
/// # Return value
///
/// A GLuint pointing to the texture.
///
/// # Example
/// ```
/// use engine::{Game, components, utils::image_loader}
///
/// let gl = game.get_gl();
///
/// game.get_mut_component_manager().add_component(entity_id, components::Texture{texture: image_loader::load_texture_from_image("texture.png", &gl)});
/// ```
///
/// # Note
///
/// To create a Texture component you can simply do :
/// ```
/// components::Texture::new("texture.png", &gl);
/// ```
pub fn load_texture_from_image(location: &str, gl: &Gl) -> gl::types::GLuint{
    let path = Path::new(location);
    let data = open(path).unwrap().into_rgb();
    let image = data.as_flat_samples();
    let texture: gl::types::GLuint = 0;
    unsafe{
        gl.GenTextures(1, texture as *mut gl::types::GLuint);
        gl.BindTexture(gl::TEXTURE_2D, texture);
        gl.TexImage2D(
            gl::TEXTURE_2D,
            0,
            gl::RGB as i32,
            image.layout.width as gl::types::GLsizei,
            image.layout.height as gl::types::GLsizei,
            0,
            gl::RGB,
            gl::UNSIGNED_BYTE,
            image.as_slice().as_ptr() as *const core::ffi::c_void,
        );
        gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MAG_FILTER, gl::LINEAR as i32);
        gl.TexParameteri(gl::TEXTURE_2D, gl::TEXTURE_MIN_FILTER, gl::LINEAR_MIPMAP_LINEAR as i32);
        gl.GenerateMipmap(gl::TEXTURE_2D);
    }
    texture
}
