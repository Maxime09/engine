use render_lib::{Object3D, VertexTrait, na, Program, gl};
use std::ffi::CString;
use ecs::Component;
use std::collections::HashMap;


pub struct ProjMatrix{
    pub mat: na::Matrix4<f32>,
}

impl ProjMatrix{
    pub fn set(&mut self, mat: na::Matrix4<f32>){
        self.mat = mat;
    }
    pub fn get(&self) -> na::Matrix4<f32>{
        self.mat
    }
}

impl Component for ProjMatrix{}

pub struct GlComp {
    pub gl: gl::Gl,
}

impl GlComp{
    pub fn get(&self) -> &gl::Gl{
        &self.gl
    }
}

impl Component for GlComp{}

pub struct ProgramComp{
    pub programs: HashMap<&'static str, Program>
}

impl ProgramComp{
    pub fn get(&self, key: &str) -> &Program{
        self.programs.get(key).expect("Trying to access to a non-existent shader")
    }

    pub fn add_shader(&mut self, key: &'static str, shader: Program){
        self.programs.insert(key, shader);
    }
}

impl Component for ProgramComp{}

/*
old function not used anymore
pub fn render<T>(objects: &[&Object3D<T>], projection: na::Matrix4<f32>, view: na::Matrix4<f32>, program: &Program, gl: &gl::Gl) where T: VertexTrait + Clone{
    let mvp_id = program.get_uniform_location(gl, CString::new("MVP").unwrap());
    let vp_matrix = projection * view;
    for object in objects{
        let mvp = vp_matrix * (*object).get_model_matrix();
        render_lib::render_object(*object, &mvp, program, mvp_id, gl);
    }
}
*/

pub fn render_v<T>(objects: &[Object3D<T>], projection: na::Matrix4<f32>, view: na::Matrix4<f32>, program: &Program, gl: &gl::Gl) where T: VertexTrait + Clone{
    let mvp_id = program.get_uniform_location(gl, CString::new("MVP").unwrap());
    let vp_matrix = projection * view;
    for object in objects{
        let mvp = vp_matrix * object.get_model_matrix();
        render_lib::render_object(object, &mvp, program, mvp_id, gl);
    }
}
