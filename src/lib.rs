#[macro_use]
extern crate render_gl_derive;

pub mod game;
pub mod components;
pub mod systems;
pub mod vertices;
pub mod utils;

pub use game::Game;
pub use render_lib;
pub use ecs;
#[cfg(feature = "physics")]
pub use ncollide3d::shape;

mod render;
mod shader;
