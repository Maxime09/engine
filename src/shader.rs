use render_lib::render_gl::Shader;
use render_lib::gl;
use std::ffi::CString;

///Return the default vertex shader to use with color vertices
pub fn color_vertex_vertex_shader(gl: &gl::Gl) -> Shader{
    let string_source = CString::new(include_str!("shaders/color_vertex/vertex.glsl")).unwrap();
    let source = string_source.as_c_str();
    Shader::from_vert_source(gl, source)
}

///Return the default fragment shader to use with color vertices
pub fn color_vertex_fragment_shader(gl: &gl::Gl) -> Shader{
    let string_source = CString::new(include_str!("shaders/color_vertex/fragment.glsl")).unwrap();
    let source = string_source.as_c_str();
    Shader::from_frag_source(gl, source)
}

///Return the default vertex shader to use with UV vertices
pub fn uv_vertex_vertex_shader(gl: &gl::Gl) -> Shader{
    let string_source = CString::new(include_str!("shaders/uv_vertex/vertex.glsl")).unwrap();
    let source = string_source.as_c_str();
    Shader::from_vert_source(gl, source)
}

///Return the default fragment shader to use with UV vertices
pub fn uv_vertex_fragment_shader(gl: &gl::Gl) -> Shader{
    let string_source = CString::new(include_str!("shaders/uv_vertex/fragment.glsl")).unwrap();
    let source = string_source.as_c_str();
    Shader::from_frag_source(gl, source)
}
