#version 330

layout(location = 0) in vec3 Position;
layout(location = 1) in vec2 UV;

uniform mat4 MVP;

out VS_OUTPUT {
  vec2 UV;
} OUT;

void main()
{
  gl_Position = MVP * vec4(Position, 1.0);
  OUT.UV = UV;
}
