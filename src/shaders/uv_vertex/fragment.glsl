#version 330 core

in VS_OUTPUT {
  vec2 UV;
} IN;

out vec4 Color;

uniform sampler2D textureSampler;

void main(){
    Color = vec4(texture(textureSampler, IN.UV ).rgb, 1.0f);
}
