use render_lib::{
    vertex::VertexTrait,
    render_gl::data,
    gl
};

///Represent a vertex in 3d space with coordinates on texture
#[derive(VertexAttribPointers, Copy, Clone, Debug)]
#[repr(C, packed)]
pub struct Vertex {

    ///The position of the vertex
    #[location = 0]
    pub pos: data::f32_f32_f32,

    ///The position of the vertex on the texture (values must be between 0 and 1).
    #[location = 1]
    pub uv: data::f32_f32,
}

impl Vertex {
    /// Create a single vertex
    ///
    /// # Arguments
    /// * `x`, `y` and `z` representing the x, y and z coordinates of the vertex in the 3d space
    ///
    /// * `uv_x`, `uv_y` representing the coordinate on the texture (between 0 and 1)
    pub fn new(x: f32, y: f32, z: f32, uv_x: f32, uv_y: f32) -> Self{
        Vertex{
            pos: (x, y, z).into(),
            uv: (uv_x, uv_y).into(),
        }
    }

    /// see [Vertex::new()](#method.new)
    pub fn from_tuple(pos: (f32, f32, f32), uv:(f32, f32)) -> Self{
        Vertex{
            pos: pos.into(),
            uv: uv.into(),
        }
    }
}
